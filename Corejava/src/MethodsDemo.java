
public class MethodsDemo {

	int z = 10;
	public void methodName(){
		
		System.out.println("Method one");
	}
	
	public int getValue(){
		System.out.println("Get Value method");
		
		int x = 10;
		return x;
	}
	
	public MethodsDemo getObject(){
		System.out.println("Get Object method");
		//return null;
		
		MethodsDemo obj1=new MethodsDemo();
		obj1.z = 30;
		
		return obj1;
	}
	
	public int add(int x,int y){
		
		return x+y;
	}
	
	public void changeObject(MethodsDemo obj){
		obj.z = 100;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub 
		MethodsDemo obj=new MethodsDemo();
		obj.methodName();
		
		int y = obj.getValue();
		System.out.println("Y value is"+y);
		
		MethodsDemo x = obj.getObject();
		
		System.out.println(x.z);
		
		MethodsDemo obj2=new MethodsDemo();
		System.out.println("OBJ2 z value is"+obj2.z);
		obj.changeObject(obj2);
		System.out.println("OBJ2 z value is"+obj2.z);
		
		int c=obj.add(10, 20);
		
		System.out.println(c);
	}

}
