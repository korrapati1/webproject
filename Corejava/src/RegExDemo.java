import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExDemo {

	public static void main(String[] args) {
		
		int count = 0;
		Pattern p=Pattern.compile("[7-9][0-9]{9}");
		Matcher m=p.matcher("abaacd9");
		while(m.find())
		{
			count++;
			System.out.println(m.start()+"....."+m.end()+"...."+m.group());
		}
		
		String s =new String("ABC");
		
		Class c=s.getClass();
		System.out.println(c.getName());

	}

}
