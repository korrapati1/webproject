class Test{
	int id;
}
public class CallByReference {

	public String getName(){
		
		return "Lalitha";
	}
	
	
	public void modify(Test obj){
		
		obj.id = 100;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		CallByReference obj=new CallByReference();
		Object name=obj.getName();
		System.out.println(name);
		System.out.println(obj.getName());
		
		Test o=new Test();
		o.id = 20;
		
		System.out.println(o.id);
		obj.modify(o);
		
		System.out.println(o.id);
		
		
	}

}
