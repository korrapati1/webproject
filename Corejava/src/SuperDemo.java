import java.lang.Enum;

class MyPerson
{
	String name;
	MyPerson(String name){
		System.out.println("MyPerson COnstructor");
		this.name = name;
	}
	
	public String getName(){
		
		return name;
	}
}

class Manager extends MyPerson
{
	Manager(String name){
		
		super(name);
	}
}

enum Beer{
	KF,RC,KO
}


public class SuperDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Manager obj=new Manager("Lalitha");
		
		String n=obj.getName();
		
		Beer[] b=Beer.values();
		
		int[] a ={1,2,3};
		for(int c:a){
			System.out.println(c);
		}

	}

}
